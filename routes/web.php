<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WelcomeController::class, 'welcome']);

Route::get('/products/{product}', [WelcomeController::class, 'productDetails'])->name('frontend.products.show');

require __DIR__ . '/auth.php';
Route::middleware('auth')->prefix('dashboard')->group(function () {
    Route::get('/home', function () {
        return view('home');
    });
    Route::resource('products', ProductController::class);
});