<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Image;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::latest()->get();
        return view('products.index', compact('products'));
    }

    public function create()
    {
        return view('products.create');
    }

    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    public function store(Request $request)
    {
        $requestData = [
            'category_name' => $request->category_name,
            'brand_name' => $request->brand_name,
            'name' => $request->name,
            'description' => $request->description,
            'image' => $this->uploadImage($request->file('image'))
        ];

        $product = Product::create($requestData);

        return redirect()
            ->route('products.index')
            ->withMessage('Successfully Created');
    }

    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        // $products = Product::find($id);

        $requestData = [
            'category_name' => $request->category_name,
            'brand_name' => $request->brand_name,
            'name' => $request->name,
            'description' => $request->description,
        ];

        if ($request->hasFile('image')) {
            $requestData['image'] = $this->uploadImage($request->file('image'));
        }

        $product->update($requestData);

        return redirect()
            ->route('products.index')
            ->withMessage('Successfully Updated');
    }


    public function destroy(Product $product)
    {

        $product->delete();
        return redirect()
            ->route('products.index')
            ->withMessage('Successfully deleted');
    }



    public function uploadImage($image)
    {
        $originalName = $image->getClientOriginalName();
        $fileName = date('Y-m-d') . time() . $originalName;

        // $image->move(storage_path('/app/public/products'), $fileName); 

        Image::make($image)
            ->resize(200, 200)
            ->save(storage_path() . '/app/public/products/' . $fileName);

        return $fileName;
    }
}