<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function welcome()
    {
        if ($keyword = request('keyword')) {
            $products = Product::latest()
                ->where('title', 'LIKE', "%{$keyword}%")
                ->get();
        } else {
            $products = Product::latest()->paginate(15);
        }


        // $carousels = Carousel::find();
        // dd($carousels);
        return view('welcome', compact('products'));
    }


    public function productDetails(Product $product)
    {
        return view('product', compact('product'));
    }
}