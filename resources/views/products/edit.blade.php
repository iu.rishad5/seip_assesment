<x-master>
    <x-slot:title>
        Product Create
        </x-slot>

        <x-forms.message />

        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">{{ __('Products') }}</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="{{ route('products.index') }}">
                    <button type="button" class="btn btn-sm btn-outline-info">
                        <span data-feather="list"></span>
                        {{ __('List') }}
                    </button>
                </a>
            </div>
        </div>

        <x-forms.errors />

        <form action="{{ route('products.update', $product->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('patch')


            <x-forms.input type="text" name="category_name" :value="old('category_name', $product->category_name)" placeholder="Enter category name"
                required label="Category Name" />
            <x-forms.input type="text" name="brand_name" :value="old('brand_name', $product->brand_name)" placeholder="Enter name" required
                label="Brand Name" />
            <x-forms.input type="text" name="name" :value="old('name', $product->name)" placeholder="Enter name" required
                label="Name" />


            <x-forms.input type="file" name="image" label='Picture' />

            <x-forms.textarea name="description" label="Description" :value="old('description', $product->description)" />

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

</x-master>
