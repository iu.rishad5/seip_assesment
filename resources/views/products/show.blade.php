<x-master>
    <x-slot:title>
        Product Details
        </x-slot>

        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Product</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="{{ route('products.index') }}">
                    <button type="button" class="btn btn-sm btn-outline-primary">
                        <span data-feather="list"></span>
                        List
                    </button>
                </a>
            </div>
        </div>

        <p>Category: {{ $product->category_name }}</p>
        <p>Brand: {{ $product->brand_name }}</p>
        <h1>Title: {{ $product->name }}</h1>
        <p>Description:{{ $product->description }}</p>
        <img src="{{ asset('storage/products/' . $product->image) }}" />
</x-master>
