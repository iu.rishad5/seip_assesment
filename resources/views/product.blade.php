<x-frontend.master>


    <div class="container marketing">
        <br><br><br><br>
        <!-- Three columns of text below the carousel -->
        <div class="row">
            <div class="col-lg-4 mb-2">
                <div class="card">
                    <div class="card-header">
                        <img height="350" src="{{ asset('storage/products/' . $product->image) }}"
                            alt="{{ $product->title }}" />
                    </div>
                </div>
            </div><!-- /.col-lg-4 -->

            <div class="col-lg-8 mb-2">
                <div class="card">
                    <div class="card-header">
                        <h3>{{ Str::limit($product->name, 40) }}</h3>
                    </div>
                    <div class="card-body">
                        <h6>Category: {{ Str::limit($product->category_name, 40) }}</h6>
                        <h6>Brand: {{ Str::limit($product->brand_name, 40) }}</h6>
                        <p>{{ Str::limit($product->description, 50) }}</p>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-primary btn-sm" href="#">Add to Card</a>
                    </div>
                </div>
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->
</x-frontend.master>
